- [ML Journey](#ml-journey)
  - [Types of ML](#types-of-ml)
  - [Python Handbook - Colab Notes](#python-handbook---colab-notes)
  - [ML Exercises](#ml-exercises)
  - [Sources](#sources)
    - [General Reads](#general-reads)
    - [Next](#next)
  
# ML Journey

## Types of ML
- Supervised ML:
  Driving a Relationship between desires and label
    - Classification problems: Predicting labels bases on two or more Discrete categories
    - Regression problems: Predicting continuous labels
- Unsupervised ML:
    1. Modeling without reference to any labels
    1. Identifying structure in unlabeled data.
       - Clustering: Identify distinct groups
       - Dimensionality reduction:
             1.Search for more succinct representations of the data
             1. Detect and identify lower dimensional structure in higher dimensional data.
- Semi-Supervised ML: Useful when only incomplete labels are available
  - Feedback reward models
  - Autonomous driving
- Neural Networks
  - [Types of Neural Networks](https://towardsdatascience.com/types-of-neural-network-and-what-each-one-does-explained-d9b4c0ed63a1)

- Modelling
  - Choose an estimator class
    - Generative Models
      - Naive Bayes - Classifier
      - Gaussian Naive Bayes - sklearn.naive_bayes.GaussianNB
      - Multinomial NB - sklearn.naive_bayes.MultinomialNB
    - Linear Regression - Regressor
      - Simple Linear Regression
      - Basis Function Regression
      - Polynomial basis functions - **sklearn.preprocessing.PolynomialFeatures**
      - Gaussian basis functions
    - Decision Trees
      - Random Forest
      - XGBoost
        - [Intro](https://datawookie.netlify.com/blog/2015/12/installing-xgboost-on-ubuntu/)
        - [Favorita](https://www.kaggle.com/sohinibhattacharya86/predict-grocery-sales-rf-xgb)
        - [Kaggle-another](https://www.kaggle.com/beiwenwu/xgboost-in-python-with-rmspe/code)
```
      from sklearn.base import BaseEstimator, TransformerMixin

      class GaussianFeatures(BaseEstimator, TransformerMixin):
      # Uniformly spaced Gaussian features for one-dimensional input

        def __init__(self, N, width_factor=2.0):
            self.N = N
            self.width_factor = width_factor

        @staticmethod
        def _gauss_basis(x, y, width, axis=None):
            arg = (x - y) / width
            return np.exp(-0.5 * np.sum(arg ** 2, axis))

        def fit(self, X, y=None):
            # create N centers spread along the data range
            self.centers_ = np.linspace(X.min(), X.max(), self.N)
            self.width_ = self.width_factor * (self.centers_[1] - self.centers_[0])
            return self

        def transform(self, X):
            return self._gauss_basis(X[:, :, np.newaxis], self.centers_,
                                     self.width_, axis=1)
        gauss_model = make_pipeline(GaussianFeatures(20),
                                    LinearRegression())
        gauss_model.fit(x[:, np.newaxis], y)
        yfit = gauss_model.predict(xfit[:, np.newaxis])
        plt.scatter(x, y)
        plt.plot(xfit, yfit)
        plt.xlim(0, 10)
```
  - Regularization
    - Ridge Regression (L2 Regularization) - from sklearn.linear_model.Ridge
    - Lasso Regression (L1 Regularization)
- Discriminative classification
  - Support Vector Classifier - sklearn.svm.SVC
- Choose model hyper-parameters by instantiating the class with desired values.
- Prep the data
  - features, labels
    - ?? [Importance of Feature Scaling](https://scikit-learn.org/stable/auto_examples/preprocessing/plot_scaling_importance.html)
  - Train, test, split
  - Feature Engineering
    - Categorial - sklearn.feature_extraction.DictVectorizer
    - Text
      - sklearn.feature_extraction.text.CountVectorizer
      - sklearn.feature_extraction.text.TfidfVectorizer
    - Derived - sklearn.preprocessing.PolynomialFeatures
    - Imputing - sklearn.impute.SimpleImputer
    - Pipelines - sklearn.pipeline.make_pipeline
- Fit the model
  - Hyperparameters and Validation
    - sklearn.learning_curve.validation_curve
    - sklearn.learning_curve.learning_curve
    - sklearn.model_selection.GridSearchCV
- Predict by applying to new data
## Python Handbook - Colab Notes
[**Book Link - Python DataScience Handbook**](https://github.com/jakevdp/PythonDataScienceHandbook)
- [Note 1: Book Intro](https://github.com/gaberm/ML-Journey/blob/master/PyHandbook/PyHandbook%20-%20Note1.ipynb)
- [Note 2: SciKit intro](https://github.com/gaberm/ML-Journey/blob/master/PyHandbook/PyHandbook%20-%20Note2.ipynb)
- [Note 3: Hyperparameters and Validation](https://github.com/gaberm/ML-Journey/blob/master/PyHandbook/PyHandbook%20-%20Note3.ipynb)
- [Note 4: Feature Engineering](https://github.com/gaberm/ML-Journey/blob/master/PyHandbook/PyHandbook%20-%20Note4.ipynb)
- [Note 5: Naive Bayes Classification](https://github.com/gaberm/ML-Journey/blob/master/PyHandbook/PyHandbook%20-%20Note5_%20Naive%20Bayes%20Classification.ipynb)
- [Note 6: Linear Regression](https://github.com/gaberm/ML-Journey/blob/master/PyHandbook/PyHandbook%20-%20Note6_%20Linear%20Regression.ipynb)
- [Note 7: Support Vector Machines](https://github.com/gaberm/ML-Journey/blob/master/PyHandbook/PyHandbook_Note7_SVM.ipynb)
- [Note 8: Decision Trees & Random Forests](https://github.com/gaberm/ML-Journey/blob/master/PyHandbook/PyHandbook_Note8_DT_%26_RandomForest.ipynb)
- [Note 9: PCA & Manifold](https://github.com/gaberm/ML-Journey/blob/master/PyHandbook/PyHandbook_Note9_PCA_and_Manifold.ipynb)
## ML Exercises
- My Titanic <https://www.kaggle.com/mohamedg/titanic-training>

## Sources
- Getting Started
  - [A good intro to Python](https://jakevdp.github.io/WhirlwindTourOfPython/index.html)
  - [Probability](https://medium.com/brandons-computer-science-notes/an-introdcution-to-probability-45a64aee7606)
  - [ML Fundamentals](https://hackernoon.com/absolute-fundamentals-of-machine-learning-dca5deee78df)
  - [JakeVdp ML Tutorial](https://nbviewer.jupyter.org/github/jakevdp/sklearn_tutorial/blob/master/notebooks/Index.ipynb)
  - [Should I normalize/standardize/rescale ?](http://www.faqs.org/faqs/ai-faq/neural-nets/part2/section-16.html)
  - [How to Learn DS for Free](https://medium.com/m/global-identity?redirectUrl=https%3A%2F%2Ftowardsdatascience.com%2Fhow-to-learn-data-science-for-free-eda10f04d083%3Futm_source%3Dlinkedin%26utm_medium%3Dmatillion)
  - [NumPy](https://numpy.org/)
  - [Pandas](https://pandas.pydata.org/pandas-docs/stable/getting_started/10min.html)
  - [SciKit Learn](https://scikit-learn.org/stable/index.html)
  - Online ML Competations - [Kaggle](www.kaggle.com)
  - Online ML Computation - [Google Colab](colab.research.google.com)

      Free Notebooks with Strong resources and clean interfac
  - Learn ML Youtube Course - <https://youtu.be/Cr6VqTRO1v0>
- Further reads:
  - The [Scikit-Learn](http://scikit-learn.org/) website : The Scikit-Learn website has an impressive breadth of documentation and examples covering some of the models discussed here, and much, much more. If you want a brief survey of the most important and often-used machine learning algorithms, this website is a good place to start.
  - SciPy, PyCon, and PyData tutorial videos: Scikit-Learn and other machine learning topics are perennial favorites in the tutorial tracks of many Python-focused conference series, in particular the PyCon, SciPy, and PyData conferences. You can find the most recent ones via a simple web search.
  - [Introduction to Machine Learning with Python](http://shop.oreilly.com/product/0636920030515.do):

      Written by Andreas C. Mueller and Sarah Guido, this book includes a fuller treatment of the topics in this chapter. If you're interested in reviewing the fundamentals of Machine Learning and pushing the Scikit-Learn toolkit to its limits, this is a great resource, written by one of the most prolific developers on the Scikit-Learn team.
  - [Python Machine Learning](https://www.packtpub.com/big-data-and-business-intelligence/python-machine-learning):

      Sebastian Raschka's book focuses less on Scikit-learn itself, and more on the breadth of machine learning tools available in Python. In particular, there is some very useful discussion on how to scale Python-based machine learning approaches to large and complex datasets.

### General Reads
  - [StatQuest](https://statquest.org/video-index/) - Video tutorials
  - Machine Learning - [coursera](https://www.coursera.org/learn/machine-learning):

    Taught by Andrew Ng (Coursera), this is a very clearly-taught free online course which covers the basics of machine learning from an algorithmic perspective. It assumes undergraduate-level understanding of mathematics and programming, and steps through detailed considerations of some of the most important machine learning algorithms. Homework assignments, which are algorithmically graded, have you actually implement some of these models yourself.
  - [Pattern Recognition and Machine Learning](http://www.springer.com/us/book/9780387310732):

      Written by Christopher Bishop, this classic technical text covers the concepts of machine learning discussed in this chapter in detail. If you plan to go further in this subject, you should have this book on your shelf.
  - [Machine Learning: a Probabilistic Perspective](https://mitpress.mit.edu/books/machine-learning-0) :

      Written by Kevin Murphy, this is an excellent graduate-level text that explores nearly all important machine learning algorithms from a ground-up, unified probabilistic perspective.
### Next
- Probability: 
  - [Intro to Probability](https://medium.com/brandons-computer-science-notes/an-introdcution-to-probability-45a64aee7606)
  - [Common Scoring Functions](https://scikit-learn.org/stable/modules/model_evaluation.html) - (SciKit)
  Functions ending with score should be maximized, ending with loss or error should be minimized
  - Cost & Metrics functions
    - [Regression metrics](https://scikit-learn.org/stable/modules/model_evaluation.html#regression-metrics)
    - Intro to Cost Functions - VSeries
      - [Part1. Intro to Cost Functions](https://www.youtube.com/watch?v=euhATa4wgzo)
      - [Part2. Least Squares Deviation Cost for Regression](https://www.youtube.com/watch?v=iSfcRku6euQ)
      - [Part3. Least Absolute Deviation And Huber M Cost](https://www.youtube.com/watch?v=QV0j4q2gQg0)
      - [Part4. Introduction To Binary Classification](https://www.youtube.com/watch?v=T5zJHhTO1FA)   --> I'm here
  - [Loss Functions](https://github.com/VowpalWabbit/vowpal_wabbit/wiki/Loss-functions)
  - [Kaggle Ensemble Guide](https://mlwave.com/kaggle-ensembling-guide/)
