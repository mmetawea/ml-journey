## Reads

* [A Standard Multivariate, Multi-Step, and Multi-Site Time Series Forecasting Problem](https://machinelearningmastery.com/standard-multivariate-multi-step-multi-site-time-series-forecasting-problem/)
  
* Air Pullotion competition - Winner [Ben Hamner](https://www.linkedin.com/in/ben-hamner-98759712/), described his winning approach in the post “[Chucking everything into a Random Forest: Ben Hamner on Winning The Air Quality Prediction Hackathon](http://blog.kaggle.com/2012/05/01/chucking-everything-into-a-random-forest-ben-hamner-on-winning-the-air-quality-prediction-hackathon/)” and provided his code on [GitHub](https://github.com/benhamner/Air-Quality-Prediction-Hackathon-Winning-Model).

* Forum Discussion - [General approaches to partitioning the models?](https://www.kaggle.com/c/dsg-hackathon/discussion/1821).

* [Time Series Forecasting with the Long Short-Term Memory Network in Python](https://machinelearningmastery.com/time-series-forecasting-long-short-term-memory-network-python/)

**Series** - [Deep Learning Time Series](https://machinelearningmastery.com/category/deep-learning-time-series/)

## Videos 
* [Two Effective Algorithms for Time Series Forecasting](https://www.youtube.com/watch?v=VYpAodcdFfA)

* seq2seq Technique - 
  * https://google.github.io/seq2seq/nmt/
  * https://towardsdatascience.com/seq2seq-model-in-tensorflow-ec0c557e560f

## PyTorch - 
https://pytorch.org/

https://pytorch.org/tutorials/beginner/deep_learning_60min_blitz.html
## Prophet - 
https://facebook.github.io/prophet/

https://research.fb.com/blog/2017/02/prophet-forecasting-at-scale/

https://www.digitalocean.com/community/tutorials/a-guide-to-time-series-forecasting-with-prophet-in-python-3

## Keras
https://github.com/keras-team/keras/tree/master/examples
